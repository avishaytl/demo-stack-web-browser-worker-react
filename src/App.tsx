import './App.css';
import { AppStyle } from './styles/styles'
import PadsGridView from './components/PadsGridView';
import { StoreProvider } from './stores';

function App() { 
  return (
    <StoreProvider>
      <AppStyle.Container>
        <img 
          src={'https://assets-global.website-files.com/607f1c20cb8fe441b6d4f7b9/61007297e4ed4f2a460d35a3_logo.png'} 
          className={'img'} 
          alt={'logol'} />
          <PadsGridView/>
      </AppStyle.Container> 
    </StoreProvider>
  );
}

export default App;
