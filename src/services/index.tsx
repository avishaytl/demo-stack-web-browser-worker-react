import services from "./services";

const useServices = () =>{
    const appServices = services();
    return appServices
}

export default useServices