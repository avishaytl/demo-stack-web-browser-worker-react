
const translation: Translation = {
    addNew: 'Add New Pad',
    stopAll: 'Stop All',
    stopLooper: 'Stop Looper',
    resetAll: 'Reset All Pads'
}

export  interface Translation {
    addNew: string,
    stopAll: string,
    stopLooper: string,
    resetAll: string
}

export interface Services {
    translation: Translation,
    getRandomString: (length: number) => string
}
  

const services = ():Services =>{
    return({
        translation: translation,
        getRandomString(length: number):string {
            var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var result: string = '';
            for ( var i = 0; i < length; i++ ) {
                result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
            }
            return result;
        },
    })
}

export default services