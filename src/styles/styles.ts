import styled from "styled-components";

export const AppStyle = {
    Container: styled.div` 
        width: 100vw;
        height: 100vh;
        overflow: hidden;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        background: #333;
    `,
    Button: styled.button`
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        border-radius: 5px;
        cursor: pointer;
        padding: 5px;
        margin: 5px;
        background: #cecece;
    `,
    TopButtonsBar: styled.div`
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
    `,
} 

export const GridStyle = {
    Container: styled.div`  
        overflow: hidden;
        display: grid;
        grid-template-columns: auto auto auto;
        align-items: center;
        justify-items: center;
        overflow: auto;
    `,
    GridPadItem:  styled.button<{background: string}>` 
        width: 100px;
        max-width: calc(100vw / 3.3);
        height: 100px;
        cursor: pointer;
        background: #333;
        margin: 10px;
        border: none;
        border-radius: 10px;
        box-shadow: -5px -5px 15px #333, 5px 5px 15px #222, inset 5px 5px 10px #333, inset -5px -5px 10px #333;
        color: #222;
        font-size: 16px;

        &:hover {
            box-shadow: -5px -5px 15px #333, 5px 5px 15px #222, inset 5px 5px 10px #222, inset -5px -5px 10px #444;
            font-size: 15px;
            transition: all 500ms;
        }

        &:focus {
            outline: none;
        }
        p {
            color: ${p => p.background};
            height: 100%;
            padding-top: 25%;
            font-weight: bold;
        }
        p:hover {
            color: #93f071;
            text-shadow: 0px 0px 10px #93f071;
        }
    `,
    GridStopButton:  styled.div`
        color: #FFF;
        text-align: center;
        color: rgba(255, 255, 255, 0.85);
        font-family: "Lato", Verdana, sans-serif;
        font-size: 20px;  
        padding: 15px;
        border-radius: 120px;
        background: radial-gradient(#EF495A, #DE2839);
        box-shadow: 1px 2px 1px rgba(255, 255, 255, 0.15) inset;
        transition: all 2s ease;
        &:hover {
            box-shadow: 1px 3px 1px #CE152A;
            cursor: pointer;
            color: rgba(255, 255, 255, 0.75);
        }
    `,
} 
 