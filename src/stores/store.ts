import appServices from "../services/index";
import WorkerBuilder from "../workers/worker_builder";
import DemoWorker from '../workers/demo_worker';

export interface Sound {
  sound: typeof Audio | any,
  playing: boolean,
} 

export interface SoundList {
  [key: string]: Sound
} 

export interface PadWorker {
    [key: string]: WorkerBuilder | null;
}

export interface WorkerQueue {
    key: string,
    setCount: (num: number)=> void
}

export interface Store {   
    pads: PadWorker,
    addNedPad: () => void,
    addNewWorker: (key: string) => void,
    stopAllWorkers: () => void,
    workerQueue: WorkerQueue[],
    startWorker: (key: string, setCount: (num: number)=>void) => void,
    stopWorker: (key: string) => void
    addToWorkerQueue: (key: string, setCount: (num: number)=> void) => void,
    checkWorkerQueue: () => void,
    removeFromWorkerQueue: () => void
    resetAllPads: () => void,
    removeWorkerFromQueue: (key: string) => void,
    sounds: SoundList,
    soundsAmount: number,
    createSoundList: () => void,
    toggleSound: (key: string) => void,
    stopAllSounds: () => void
}

export function store():Store { 
    const services = appServices();
    return { 
        pads: {},
        workerQueue: [],
        addNedPad(){  
            let key: string = services.getRandomString(5)
            this.pads[key] = null 
        },  
        addNewWorker(key: string){ 
            this.pads[key] = new WorkerBuilder(DemoWorker);
        },
        stopAllWorkers(){
            Object.keys(this.pads).forEach((key: string)=>{
                if(this.pads[key]){
                    (this.pads[key] as any).terminate();
                    this.pads[key] = null
                }
            })
        },
        stopWorker(key: string){
          let worker = this.pads[key]
          if(worker)
            worker.terminate();
          this.pads[key] = null
          this.removeFromWorkerQueue()
          this.checkWorkerQueue()
        },
        startWorker(key: string, setCount: (num: number)=> void){ 
          if(typeof(Worker) !== "undefined") {
            if(this.pads[key] === null) 
                this.addToWorkerQueue(key, setCount)
            
            let worker = this.pads[key]
            if(worker){
              let pads = this.pads;
              let checkWorkerQueue = this.checkWorkerQueue;
              let removeFromWorkerQueue = this.removeFromWorkerQueue;
              worker.onmessage = function(event: any) {
                  setCount(parseInt(event.data));
                  if(parseInt(event.data) === 8){
                    let worker = pads[key]
                    if(worker)
                      worker.terminate();
                    pads[key] = null 
                    removeFromWorkerQueue()
                    checkWorkerQueue()
                  }
              };
            }
          } else {
              alert('Sorry, your browser does not support Web Workers...')
          }
        },
        removeFromWorkerQueue(){
          this.workerQueue.shift()
        },
        addToWorkerQueue(key: string, setCount: (num: number)=> void){
          if(!this.workerQueue.length)
            this.addNewWorker(key)
          this.workerQueue.push({key: key, setCount: setCount})
        },
        checkWorkerQueue(){
          if(!this.workerQueue.length) return;
          this.pads[this.workerQueue[0].key] = new WorkerBuilder(DemoWorker);
          this.startWorker(this.workerQueue[0].key, this.workerQueue[0].setCount)
        },
        resetAllPads(){
          this.stopAllWorkers()
          this.pads = {}
          this.workerQueue = []
        },
        removeWorkerFromQueue(key: string){ 
          let queue: WorkerQueue[] = []
          this.workerQueue.forEach(k => {
            if(k.key !== key)
              queue.push(k)
          }) 
          this.workerQueue = queue;
        },
        sounds: {},
        stopAllSounds(){
          Object.keys(this.sounds).forEach((key)=>{ 
            this.sounds[key].playing = false;
            if(this.sounds[key].sound){
              this.sounds[key].sound.pause();
              this.sounds[key].sound.currentTime = 0;
            }
          })
        },
        soundsAmount: 9,
        createSoundList(){ 
          this.sounds[services.getRandomString(5)] = {sound: new Audio(require(`../assets/track${this.soundsAmount}.mp3`)), playing: false}
          --this.soundsAmount;
          if(this.soundsAmount > 0)
            this.createSoundList()
        },     
        toggleSound(key: string){
          this.sounds[key].playing = !this.sounds[key].playing;
        },  
    }
  }
  
  export type AppStore = ReturnType<typeof store>