import React from 'react'
import { store, AppStore } from './store'
import { useLocalStore } from 'mobx-react' // 6.x or mobx-react-lite@1.4.0

const StoreContext = React.createContext<AppStore | null>(null)

export const StoreProvider = ({ children }: any) => {
  const str = useLocalStore(store)
  return <StoreContext.Provider value={str}>{children}</StoreContext.Provider>
}

export const useStore = () => {
  const st = React.useContext(StoreContext)
  if (!st) {
    throw new Error('useStore must be used within a StoreProvider.')
  }
  return st
}