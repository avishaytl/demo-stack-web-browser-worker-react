import { Observer } from "mobx-react";
import { useEffect } from "react";
import { GridStyle } from "../styles/styles";
 
interface PadGridItemProps {
  workerKey: string, 
  sound: any, 
  playing: boolean, 
  toggleSound: (workerKey: string)=> void
}
const PadGridItem = ({workerKey, sound, playing, toggleSound}: PadGridItemProps) => { 
      useEffect(() => {
        if(playing){ 
          sound.loop = true;
          sound.play();
        }else{  
          sound.pause();
          sound.currentTime = 0;
        }
      },
      [sound, playing]
    );
    const handlePress = () =>{
        toggleSound(workerKey)
    }
    return(
      <Observer>
        {()=><GridStyle.GridPadItem background={playing ? '#46ff43' : '#5a0e0e'} onClick={handlePress} key={workerKey}>
	        <p>{workerKey}</p>
        </GridStyle.GridPadItem>}
      </Observer>
    )
}

export default PadGridItem