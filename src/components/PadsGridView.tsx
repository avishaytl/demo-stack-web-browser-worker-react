import { GridStyle } from "../styles/styles"
import PadGridItem from "./PadGridItem";
import { useStore } from "../stores";
import { useEffect } from "react";
import { Observer } from "mobx-react";

const PadsGridView = () =>{ 
    const store = useStore();
    useEffect(()=>{
        store.createSoundList()
    },[])
    return( 
        <>
            <GridStyle.GridStopButton onClick={store.stopAllSounds}>
                STOP
            </GridStyle.GridStopButton>
            <Observer>
                {()=><GridStyle.Container> 
                        {store.soundsAmount ? null : Object.keys(store.sounds).map((key)=>{
                            if(!store.sounds[key].sound) return null
                            return(<PadGridItem 
                                        toggleSound={store.toggleSound} 
                                        playing={store.sounds[key].playing} 
                                        sound={store.sounds[key].sound} 
                                        workerKey={key} 
                                        key={key}/>)
                        })}
                    </GridStyle.Container>}
            </Observer>
        </>
    )
}

export default PadsGridView